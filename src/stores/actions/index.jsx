// Init
export const ACTIONS_INIT             = 'ACTIONS_INIT';


export class Actions {
    static createActionInit(data) {
        return {
            type: ACTIONS_INIT,
            payload: data
        }
    }
}

