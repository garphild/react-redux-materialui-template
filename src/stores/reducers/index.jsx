import { combineReducers } from 'redux';

import rootReduser from 'stores/reducers/rootReduser.jsx';

export default combineReducers({
    root: rootReduser,
});

