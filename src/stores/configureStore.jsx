import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';
import rootReducer from './reducers'

export default function configureStore(initialState) {
        
        const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        var middlewares = [
            thunk
        ];
        const store = createStore(rootReducer, /* preloadedState, */ composeEnhancers(
                applyMiddleware(...middlewares)
        ));
    
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers')
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}

