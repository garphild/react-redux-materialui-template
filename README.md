This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Use:

[React](https://reactjs.org/)

[Redux](https://redux.js.org/)

[Material-UI](https://material-ui.com)

[Material Design Icons](https://material.io/tools/icons/?style=baseline)